#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "qaesmgr.h"
#include "qecdhmgr.h"
#include <QDebug>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ecdh();

    aes();
    
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::ecdh()
{
    QECDHMgr qECDHMgrA,qECDHMgrB;
    QString pubA, priA, pubB, priB;
    // *****************
    // * 【测试1】
    // * 产生A的密钥对
    // *****************
    if(qECDHMgrA.SetCurveName("secp384r1"))
    {
        if(qECDHMgrA.GenerateKeys())
        {
            pubA = qECDHMgrA.GetPublicKey();
            priA = qECDHMgrA.GetPrivateKey();
            qDebug() << "PublicKeyA" << pubA;
            qDebug() << "PrivateKeyA" << priA;
        }else
        {
            qDebug() << "GenerateKeys fail" ;
        }
    }else
    {
        qDebug() << "The curve is not supported.";
    }

    // *****************
    // * 【测试2】
    // * 产生B的密钥对
    // *****************
    if(qECDHMgrB.SetCurveName("secp384r1"))
    {
        if(qECDHMgrB.GenerateKeys())
        {
            pubB = qECDHMgrB.GetPublicKey();
            priB = qECDHMgrB.GetPrivateKey();
            qDebug() << "PublicKeyB" << pubB;
            qDebug() << "PrivateKeyB" << priB;
        }else
        {
            qDebug() << "GenerateKeys fail" ;
        }
    }else
    {
        qDebug() << "The curve is not supported.";
    }

    qDebug() << "*********************************";

    // *****************
    // * 【测试3】
    // * 分别载入公钥、私钥
    // *****************
    QECDHMgr qECDHMgrA1;
    if(qECDHMgrA1.SetCurveName("secp384r1"))
    {
        // 写入公钥
        if(qECDHMgrA1.SetPublicKey(pubA))
        {
            qDebug() << "PublicKeyA1" << qECDHMgrA1.GetPublicKey();
        }else
        {
            qDebug() << "set pubkeyA1 Error" << qECDHMgrA1.GetLastError();
        }

        // 写入私钥
        if(qECDHMgrA1.SetPrivateKey(priA))
        {
            qDebug() << "PrivateKeyA1" << qECDHMgrA1.GetPrivateKey();
            QString secA1 = qECDHMgrA1.DeriveSharedSecret(pubB);
            qDebug() << "SecKeyA1" << secA1;
        }else
        {
            qDebug() << "set prikeyA1 Error" << qECDHMgrA1.GetLastError();
        }
    }else
    {
        qDebug() << "The curve is not supported.";
    }

    qDebug() << "*********************************";


    // *****************
    // * 【测试4】
    // * 一次性载入密钥对（包含公钥和私钥）
    // *****************
    QECDHMgr qECDHMgrA2;
    if(qECDHMgrA2.SetCurveName("secp384r1"))
    {
        if(qECDHMgrA2.SetKeyPair(pubA, priA))
        {
            qDebug() << "PrivateKeyA3" << qECDHMgrA2.GetPrivateKey();
            qDebug() << "PublicKeyA3" << qECDHMgrA2.GetPublicKey();
        }else
        {
            qDebug() << "set prikeyA3 Error" << qECDHMgrA2.GetLastError();
        }
    }else
    {
        qDebug() << "The curve is not supported.";
    }


    qDebug() << "*********************************";

    // *****************
    // * 【测试5】
    // * 生成对称密钥
    // *****************
    QECDHMgr qECDHMgrA3, qECDHMgrB3;
    if(qECDHMgrA3.SetCurveName("secp384r1"))
    {
        if(qECDHMgrA3.SetKeyPair(pubA, priA))
        {
            qDebug() << "PrivateKeyA3" << qECDHMgrA3.GetPrivateKey();
            qDebug() << "PublicKeyA3" << qECDHMgrA3.GetPublicKey();
        }else
        {
            qDebug() << "set prikeyA3 Error" << qECDHMgrA3.GetLastError();
        }
    }else
    {
        qDebug() << "The curve is not supported.";
    }
    if(qECDHMgrB3.SetCurveName("secp384r1"))
    {
        if(qECDHMgrB3.SetKeyPair(pubB, priB))
        {
            qDebug() << "PrivateKeyB3" << qECDHMgrB3.GetPrivateKey();
            qDebug() << "PublicKeyB3" << qECDHMgrB3.GetPublicKey();
        }else
        {
            qDebug() << "set prikeyB3 Error" << qECDHMgrB3.GetLastError();
        }
    }else
    {
        qDebug() << "The curve is not supported.";
    }
    qDebug() << "SecKeyA3" << qECDHMgrA3.DeriveSharedSecret(pubB);
    qDebug() << "SecKeyB3" << qECDHMgrB3.DeriveSharedSecret(pubA);
}

void MainWindow::aes()
{
    QString inputStr("The Advanced Encryption Standard (AES), also known by its original name Rijndael "
                     "is a specification for the encryption of electronic data established by the U.S. "
                    "National Institute of Standards and Technology (NIST) in 2001");
    QString key("your-string-key");
    QString iv("your-IV-vector");
     
    QByteArray hashKey = QCryptographicHash::hash(key.toLocal8Bit(), QCryptographicHash::Sha256);
    QByteArray hashIV = QCryptographicHash::hash(iv.toLocal8Bit(), QCryptographicHash::Md5);
     
    qDebug() << "原始数据" << inputStr.toUtf8();
    QByteArray encodeText = QAESMgr::encrypt(inputStr.toUtf8(), hashKey, hashIV);
    qDebug() << "加密数据"<< encodeText << QAESmgr::GetLastError();
    QByteArray decodeText = QAESmgr::decrypt(encodeText, hashKey, hashIV);
    qDebug() << "解密数据"<< decodeText<< QAESmgr::GetLastError();
}

