# QT-SECURE

#### 介绍
QT版本基于OpenSSL3的ECDH密钥交换算法和AES加密算法。

其他加密算法会陆续更新

#### OpenSSL版本
3.0.8

#### QT版本
5.14.1

#### 执行效果
![Image text](https://gitee.com/yiyefangzhou24/qecdh/raw/master/result.PNG)